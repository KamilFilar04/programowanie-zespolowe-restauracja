-- Clear table
TRUNCATE TABLE `users`;
-- Import data
INSERT INTO `users` (`id`, `name`, `surname`, `password`, `username`, `address`, `data_of_birth`, `role_id`)
VALUES (1, 'Piotr', 'Długosz', 'manager', 'manager', 'Rzeszów', '2000-04-18', 1),
       (2, 'Krzysztof', 'Czereczon', 'chef', 'chef', 'Kraków', '2001-04-18', 2),
       (3, 'Krzysztof', 'Bohaczyk', 'waiter', 'waiter', 'Opole', '2002-04-18', 3),
       (4, 'Karol', 'Bury', 'warehouseman', 'warehouseman', 'Toruń', '2003-04-18', 4),
       (5, 'Adrian', 'Zawodowiec', 'chef', 'chef2', 'Kraków', '2001-04-18', 2),
       (6, 'Piotr', 'Śmieszek', 'chef', 'chef3', 'Kraków', '2001-04-18', 2),
       (7, 'Tomasz', 'Wolny', 'chef', 'chef4', 'Kraków', '2001-04-18', 2),
       (8, 'Andrzej', 'Rączy', 'chef', 'chef5', 'Kraków', '2001-04-18', 2);
