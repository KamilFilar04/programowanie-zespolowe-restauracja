--
-- Zrzut danych tabeli `meals` i `meals_products`
--
TRUNCATE TABLE `meals`;
TRUNCATE TABLE `products_meals`;

INSERT INTO `meals` (`id`, `name`, `calories`, `net_price`, `gross_price`, `average_preparation_time`, `recipe`)
VALUES (1, 'Spaghetti', '500', 50.0, 55.0, 34.5, 'Boil pasta and add tomato sauce'),
       (2, 'Leczo', '800', 50.0, 55.0, 34.5, 'Paprika, sausage'),
       (3, 'Potatoes with onion', '20', 5.0, 6.0, 1.0, 'Boil potatoes and add fried onion'),
       (4, 'Pumpkin cream', '500', 50.0, 55.0, 34.5, 'Boil pumpkin'),
       (5, 'Red wine', '500', 50.0, 55.0, 34.5, 'Just a bottle of wine'),
       (6, 'White wine', '500', 50.0, 55.0, 34.5, 'Just a bottle of wine'),
       (7, 'Cucumber and tomato salad', '500', 50.0, 55.0, 34.5, 'Cut cucumber and tomato and mix it'),
       (8, 'Multicolored pepper salad', '500', 50.0, 55.0, 34.5, 'Cut red, yellow and green paprika'),
       (9, 'Lettuce, vinegar and tomato salad', '500', 50.0, 55.0, 34.5, 'Cut lettuce, tomato and add vinegar'),
       (10, 'Fried courgette with salt', '500', 50.0, 55.0, 34.5, 'Fry courgette and add salt');

INSERT INTO `products_meals` (`meal_id`, `product_id`, `quantity`)
VALUES (1, 4, 4),
       (1, 2, 4),
       (1, 7, 4),
       (1, 18, 4),
       (2, 9, 4),
       (2, 10, 4),
       (2, 11, 4),
       (2, 7, 4),
       (2, 8, 4),
       (3, 1, 4),
(3, 3, 4),
(3, 7, 4),
(4, 16, 4),
(4, 7, 4),
(5, 21, 4),
(6, 22, 4),
(7, 2, 4),
(7, 15, 4),
(7, 7, 4),
(8, 8, 4),
(8, 9, 4),
(8, 10, 4),
(8, 11, 4),
(8, 7, 4),
(9, 12, 4),
(9, 20, 4),
(9, 2, 4),
(9, 7, 4),
(10, 17, 4),
(10, 7, 3);

