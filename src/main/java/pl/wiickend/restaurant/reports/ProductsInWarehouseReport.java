package pl.wiickend.restaurant.reports;

import pl.wiickend.pdfgenerator.Report;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.entity.Product;
import pl.wiickend.restaurant.services.entity.ProductService;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProductsInWarehouseReport extends Report {

    @Override
    public String getPdfFileName() {
        return "products_in_warehouse_report_";
    }

    @Override
    public String getPdfName() {
        return "Products in warehouse";
    }

    public void createPdf(String generatedBy) throws FileNotFoundException {
        ProductService productService = new ProductService();
        List<Product> products = productService.loadAll();
        List<Map<String,String>> mapOfProducts = new ArrayList<>();

        for (Product product:
             products) {
            mapOfProducts.add(product.toMap());
        }

        this.createDocument(Main.PDF_DEST);
        this.addTitle(getPdfName());
        this.addTableAsMap(mapOfProducts);
        this.addSpacing(15);
        this.addGeneratedBy(generatedBy);
        this.closeDocument();
    }
}
