package pl.wiickend.restaurant.utils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import pl.wiickend.restaurant.Main;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.logging.Level;

public class Dao<T> {

    private Class<T> persistentClass;

    public Dao() {
        try{
            Main.database.getSessionFactory();
            // To make it use generics without supplying the class type
            this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
                    .getGenericSuperclass()).getActualTypeArguments()[0];
        }catch (Exception e)
        {
            Main.logger.log(Level.SEVERE,"Service Error During init for class "+persistentClass.getName());
        }

        Main.logger.log(Level.INFO,"Service Created for class "+persistentClass.getName());
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    /**
     * Get entity by id
     * @param id
     * @return T - entity
     */
    public T getById(long id){
        Main.database.openCurrentSessionWithTransaction();
        T entity = (T) Main.database.getCurrentSession().load(getPersistentClass(), id);
        Main.database.closeCurrentSessionWithTransaction();
        return  entity;
    }

    /**
     * Get all entities for selected Class
     * @return List<T> - list of records
     */
    public List<T> loadAll(){
        Main.database.openCurrentSessionWithTransaction();
        List<T> data = null;
        CriteriaBuilder builder = Main.database.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(getPersistentClass());
        criteria.from(getPersistentClass());
        data = Main.database.getCurrentSession().createQuery(criteria).getResultList();
        Main.database.closeCurrentSessionWithTransaction();
        return data;
    }

    /**
     * Delete passed entity
     * @param entity - entity to delete
     */
    public void delete(T entity){
        Main.database.openCurrentSessionWithTransaction();
        Main.database.getCurrentSession().delete(entity);
        Main.database.closeCurrentSessionWithTransaction();
    }

    /**
     * Delete entity with id
     * @param id - id of entity to delete
     */
    public void delete(Long id){
        Main.database.openCurrentSessionWithTransaction();
        T entity = this.getById(id);
        Main.database.getCurrentSession().delete(entity);
        Main.database.closeCurrentSessionWithTransaction();
    }

    /**
     * Update entity in database
     * @param entity - entity to update
     */
    public void update(T entity){
        Main.database.openCurrentSessionWithTransaction();
        Main.database.getCurrentSession().update(entity);
        Main.database.closeCurrentSessionWithTransaction();
    }

    /**
     * Save entity to database
     * @param entity - entity to save
     */
    public void save(T entity){
        Main.database.openCurrentSessionWithTransaction();
        Main.database.getCurrentSession().save(entity);
        Main.database.closeCurrentSessionWithTransaction();
    }
}
