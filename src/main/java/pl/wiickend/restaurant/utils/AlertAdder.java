package pl.wiickend.restaurant.utils;

import javafx.scene.control.Alert;

public class AlertAdder {

    Alert a = new Alert(Alert.AlertType.NONE);
    Alert.AlertType type;
    String title;
    String message;

    public AlertAdder(Alert.AlertType type, String title, String message) {
        this.a.setAlertType(type);
        this.a.setTitle(title);
        this.a.setContentText(message);
        this.a.show();
    }

    public AlertAdder() {

    }
}
