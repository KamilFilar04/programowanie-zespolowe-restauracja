package pl.wiickend.restaurant.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import pl.wiickend.restaurant.Main;

import java.io.*;
import java.util.Properties;
import java.util.logging.Level;

public class Database {
    private static SessionFactory sessionFactory = null;
    private static Session currentSession = null;
    private static Transaction currentTransaction = null;

    public Database() throws Exception {
        try {
            sessionFactory = getSessionFactory();
        }catch (Exception e)
        {
            Main.logger.log(Level.SEVERE,"Error Creating a Database Connection.");
            throw new Exception("Error Creating a Database Connection.");
        }
    }

    public void openCurrentSessionWithTransaction() {
        currentSession = this.getCurrentSession();
        try {
            if (currentTransaction == null || !currentTransaction.isActive()) {
                currentTransaction = currentSession.beginTransaction();
            } else {
                throw new Exception("Somewhere exists a transaction that wasn't saved.");
            }
        } catch (Exception e) {
            Main.logger.log(Level.SEVERE, e.getMessage());
        }
    }

    public Session getCurrentSession(){
        try {
            currentSession = getSessionFactory().getCurrentSession();
        } catch (Exception e) {
            try {
                currentSession = getSessionFactory().openSession();
            } catch (Exception e2) {
                Main.logger.log(Level.SEVERE, "Error opening a database session.");
            }
        }
        return currentSession;
    }

    public void closeCurrentSession() {
        try {
            if (currentSession.isOpen())
            {
                currentSession.close();
            }
        } catch (Exception e) {
            Main.logger.log(Level.SEVERE, "Error closing a database session.");
        }
    }

    public void closeCurrentSessionWithTransaction() {
        try{
            if(currentTransaction.isActive())
            {
                currentTransaction.commit();
            }
            else
            {
                throw new Exception("Transaction wasn't opened.");
            }
        }catch (Exception e)
        {
            Main.logger.log(Level.SEVERE,e.getMessage());
        }
        this.closeCurrentSession();
    }

    /**
     * Creates a database sessionFactory
     *
     * @return sessionFactory
     * @throws Exception
     */
    public SessionFactory getSessionFactory() throws Exception {
        if (sessionFactory == null) {
            try {
                StringWriter sw = new StringWriter();

                // Create configuration
                Properties properties = new Properties();
                InputStream io = null;

                File initialFile = new File(System.getProperty("user.home") + "/.config/Restaurant/hibernate.properties");

                if (initialFile.exists()) {
                    io = new FileInputStream(initialFile);
                } else {

                    File directory = new File(System.getProperty("user.home") + "/.config/Restaurant");

                    if (!directory.exists()) {
                        directory.mkdirs();
                    }

                    initialFile.createNewFile();

                    io = getClass().getResourceAsStream("/hibernate.example");

                    ByteArrayOutputStream into = new ByteArrayOutputStream();
                    byte[] buf = new byte[io.available()];
                    for (int n; 0 < (n = io.read(buf));) {
                        into.write(buf, 0, n);
                    }
                    into.close();
                    System.out.println(new String(into.toByteArray(), "UTF-8"));

                    io.read(buf);

                    OutputStream outStream = new FileOutputStream(initialFile);
                    outStream.write(buf);
                    outStream.close();
                }

                properties.load(io);

                io.close();
                // Create SessionFactory
                sessionFactory = new Configuration().configure("hibernate.cfg.xml").mergeProperties(properties).buildSessionFactory();
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Database Session Error.");
            }
        }
        return sessionFactory;
    }
}
