package pl.wiickend.restaurant.views.controllers.meal;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.entity.MealProduct;
import pl.wiickend.restaurant.entity.Product;
import pl.wiickend.restaurant.services.entity.MealProductService;
import pl.wiickend.restaurant.services.entity.MealService;
import pl.wiickend.restaurant.services.entity.ProductService;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MealCreateNewController {

    @FXML
    private TextField name_field;

    @FXML
    private TextField prep_field;

    @FXML
    private TextArea desc_field;

    @FXML
    private Button add_meal_btn;

    @FXML
    private TableView<MealProduct> recipe_table;

    @FXML
    private TableColumn<MealProduct,String> product_col;

    @FXML
    private TableColumn<MealProduct, Float> amount_col;

    @FXML
    private ComboBox<Integer> amount_list;

    @FXML
    private ComboBox<Product> product_list;

    @FXML
    private Button add_btn;

    @FXML
    private Button edit_btn;

    @FXML
    private Button remove_btn;

    @FXML
    private Text errorText;

    @FXML
    private Button cancel;

    MealService mealService;
    ProductService productService;
    MealProductService mealProductService;

    @FXML
    void addProduct(ActionEvent event) {
        if(product_list.getSelectionModel().getSelectedItem()==null || amount_list.getSelectionModel().getSelectedItem()==null)
            return;

        MealProduct y = new MealProduct();
        y.setProduct(product_list.getSelectionModel().getSelectedItem());
        y.setQuantity(Float.valueOf(amount_list.getSelectionModel().getSelectedItem()));

        MealProduct tableProduct = recipe_table.getItems().stream().filter(new Predicate<MealProduct>() {
            @Override
            public boolean test(MealProduct mealProduct) {
                if(mealProduct.getProductName().equals(y.getProductName()))
                    return true;
                return false;
            }
        }).findFirst().orElse(null);

        recipe_table.getItems().remove(tableProduct);

        if(tableProduct!=null)
        {
            y.setQuantity((tableProduct.getQuantity()+y.getQuantity()));
        }

        recipe_table.getItems().add(y);
        recipe_table.refresh();
    }

    @FXML
    void editProduct(ActionEvent event) {
        if(product_list.getSelectionModel().getSelectedItem()==null || amount_list.getSelectionModel().getSelectedItem()==null)
            return;

        MealProduct y = new MealProduct();
        y.setProduct(product_list.getSelectionModel().getSelectedItem());
        y.setQuantity(Float.valueOf(amount_list.getSelectionModel().getSelectedItem()));

        MealProduct tableProduct = recipe_table.getItems().stream().filter(new Predicate<MealProduct>() {
            @Override
            public boolean test(MealProduct mealProduct) {
                if(mealProduct.getProductName().equals(y.getProductName()))
                    return true;
                return false;
            }
        }).findFirst().orElse(null);

        recipe_table.getItems().remove(tableProduct);

        if(tableProduct!=null) y.setQuantity(y.getQuantity());

        recipe_table.getItems().add(y);
        recipe_table.refresh();
    }

    @FXML
    void removeProduct(ActionEvent event) {
        if(recipe_table.getSelectionModel
                ().getSelectedItem()==null)
            return;

        MealProduct y = recipe_table.getSelectionModel().getSelectedItem();

        MealProduct tableProduct = recipe_table.getItems().stream().filter(new Predicate<MealProduct>() {
            @Override
            public boolean test(MealProduct mealProduct) {
                if(mealProduct.getProductName().equals(y.getProductName()))
                    return true;
                return false;
            }
        }).findFirst().orElse(null);


        if(tableProduct!=null) recipe_table.getItems().remove(tableProduct);
        recipe_table.refresh();

    }

    @FXML
    void addMeal(ActionEvent event) {
        if(name_field.getText().isEmpty() || prep_field.getText().isEmpty() ||
                desc_field.getText().isEmpty() || recipe_table.getItems().size() == 0) return;

        Meal meal = new Meal();
        meal.setName(name_field.getText());
        meal.setDescription(desc_field.getText());
        meal.setAverage_preparation_time(Float.valueOf(prep_field.getText()));
        for(int i =0;i<recipe_table.getItems().size();i++){


            Product product = recipe_table.getItems().get(i).getProduct();
            Float quantity = recipe_table.getItems().get(i).getQuantity();
            product.setInMealQuantity(quantity);
            meal.addProduct(product);

        }
        mealService.save(meal);
        Stage stage = (Stage) recipe_table.getScene().getWindow();
        stage.close();
    }


    @FXML
    void close(ActionEvent event) {
        Stage stage = (Stage) recipe_table.getScene().getWindow();
        stage.close();
    }

    @FXML
    void initialize() throws Exception {
        mealService = new MealService();
        productService = new ProductService();
        mealProductService = new MealProductService();
        product_list.setItems(FXCollections.observableList(productService.loadAll()));

        Callback<ListView<Product>, ListCell<Product>> factory = lv -> new ListCell<Product>() {
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getName());
            }

        };

        amount_list.getItems().setAll(IntStream.rangeClosed(0,50).boxed().collect(Collectors.toList()));

        product_list.setCellFactory(factory);
        product_list.setButtonCell(factory.call(null));

        product_col.setCellValueFactory(new PropertyValueFactory<MealProduct,String>("ProductName"));
        amount_col.setCellValueFactory(new PropertyValueFactory<MealProduct,Float>("quantity"));
    }
}
