package pl.wiickend.restaurant.views.controllers.user;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Role;
import pl.wiickend.restaurant.entity.User;
import pl.wiickend.restaurant.services.entity.RoleService;
import pl.wiickend.restaurant.services.entity.UserService;
import pl.wiickend.restaurant.utils.AlertAdder;
import pl.wiickend.restaurant.utils.entity.RoleConverter;

import java.sql.Date;
import java.time.LocalDate;

public class UserEditController {


    @FXML
    private TextField userName;

    @FXML
    private TextField userSurname;

    @FXML
    private TextField userUsername;

    @FXML
    private TextField userPassword;

    @FXML
    private TextField userAddress;

    @FXML
    private ComboBox<Role> userRole;

    @FXML
    private DatePicker userDateOfBirth;

    public User user = null;

    RoleService roleService = new RoleService();
    UserService userService = new UserService();

    @FXML
    void close(ActionEvent event) {
        Stage stage = (Stage) userName.getScene().getWindow();
        stage.close();
    }

    @FXML
    void saveChanges(ActionEvent event) throws Exception {
        String name = userName.getText();
        String username = userUsername.getText();
        String surname = userSurname.getText();
        String address = userAddress.getText();
        String password = userPassword.getText();
        LocalDate date = userDateOfBirth.getValue();

        if(!name.isEmpty() &&
                !username.isEmpty() &&
                !surname.isEmpty() &&
                !address.isEmpty() &&
                !password.isEmpty() &&
                userDateOfBirth != null &&
                userRole.getSelectionModel().getSelectedItem()!=null)
        {
            Date formatedDate = Date.valueOf(date);
            user.setName(name);
            user.setSurname(surname);
            user.setUsername(username);
            user.setPassword(password);
            user.setData_of_birth(formatedDate);
            user.setRole(userRole.getSelectionModel().getSelectedItem());

            userService.update(user);

            Stage stage = (Stage) userName.getScene().getWindow();
            stage.close();
            AlertAdder alert = new AlertAdder(Alert.AlertType.INFORMATION, "User Updated", "User successfully updated!");

            System.out.println("User updated!");
        } else{
            AlertAdder alertError = new AlertAdder(Alert.AlertType.ERROR, "Error", "Invalid inputs!");
            System.out.println("Error!");
        }
    }

    @FXML
    void initialize() throws Exception {
        assert userName != null : "fx:id=\"userName\" was not injected: check your FXML file 'UsersEditView.fxml'.";
        assert userSurname != null : "fx:id=\"userSurname\" was not injected: check your FXML file 'UsersEditView.fxml'.";
        assert userUsername != null : "fx:id=\"userUsername\" was not injected: check your FXML file 'UsersEditView.fxml'.";
        assert userPassword != null : "fx:id=\"userPassword\" was not injected: check your FXML file 'UsersEditView.fxml'.";
        assert userDateOfBirth != null : "fx:id=\"userDateOfBirth\" was not injected: check your FXML file 'UsersEditView.fxml'.";
        assert userAddress != null : "fx:id=\"userAddress\" was not injected: check your FXML file 'UsersEditView.fxml'.";
        assert userRole != null : "fx:id=\"userRole\" was not injected: check your FXML file 'UsersEditView.fxml'.";
        roleService.loadAll();
        userRole.setConverter(new RoleConverter());
        userRole.getItems().addAll(roleService.loadAll());
    }

    public void setUser(User user) {
        this.user = user;
        userName.setText(user.getName());
        userUsername.setText(user.getUsername());
        userSurname.setText(user.getSurname());
        userAddress.setText(user.getAddress());
        userPassword.setText(user.getPassword());
        userDateOfBirth.setValue(LocalDate.parse(String.valueOf(user.getData_of_birth())));
        userRole.setValue(user.getRole());
    }
}
