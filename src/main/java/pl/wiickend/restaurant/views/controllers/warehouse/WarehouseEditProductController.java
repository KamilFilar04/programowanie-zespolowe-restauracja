package pl.wiickend.restaurant.views.controllers.warehouse;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Product;
import pl.wiickend.restaurant.services.entity.ProductService;

import java.net.URL;
import java.sql.Date;
import java.util.Calendar;
import java.util.ResourceBundle;

public class WarehouseEditProductController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField productName;

    @FXML
    private TextField productDescription;

    @FXML
    private TextField productUnit;

    @FXML
    private TextField productNetPrice;

    @FXML
    private TextField productGrossPrice;

    private Product product;
    private ProductService productService;

    @FXML
    void closeView(ActionEvent event) {
        this.close();
    }

    void close() {
        Stage stage = (Stage) productName.getScene().getWindow();
        stage.close();
    }

    public void setProduct(Product product) {
        this.product = productService.getById(product.getId());
        productName.setText(product.getName());
        productDescription.setText(product.getDescription());
        productUnit.setText(product.getUnit());
        productNetPrice.setText(product.getNet_price().toString());
        productGrossPrice.setText(product.getGross_price().toString());
    }

    @FXML
    void updateProduct(ActionEvent event) throws Exception {
        boolean throwErr = false;

        try {
            product.setName(productName.getText());
            product.setDescription(productDescription.getText());
            product.setUnit(productUnit.getText());
            product.setNet_price(Float.valueOf(productNetPrice.getText()));
            product.setGross_price(Float.valueOf(productGrossPrice.getText()));
            product.setUpdatedAt(new Date(Calendar.getInstance().getTime().getTime()));
        }catch (Exception e)
        {
            throwErr = true;
        }

        if(
                throwErr ||
                        productName.getText().length()==0 ||
                        productUnit.getText().length()==0 ||
                        productNetPrice.getText().length()==0 ||
                        productGrossPrice.getText().length()==0 ||
                        productDescription.getText().length()==0 ||
                        Float.parseFloat(productNetPrice.getText())<0 ||
                        Float.parseFloat(productGrossPrice.getText())<0
        )
        {
            Alert a = new Alert(Alert.AlertType.NONE);
            a.setAlertType(Alert.AlertType.ERROR);
            a.show();
            return;
        }
        productService.update(product);
        this.close();
    }

    @FXML
    void initialize() {
        assert productName != null : "fx:id=\"productName\" was not injected: check your FXML file 'WarehouseEditProductView.fxml'.";
        assert productDescription != null : "fx:id=\"productDescription\" was not injected: check your FXML file 'WarehouseEditProductView.fxml'.";
        assert productUnit != null : "fx:id=\"productUnit\" was not injected: check your FXML file 'WarehouseEditProductView.fxml'.";
        assert productNetPrice != null : "fx:id=\"productNetPrice\" was not injected: check your FXML file 'WarehouseEditProductView.fxml'.";
        assert productGrossPrice != null : "fx:id=\"productGrossPrice\" was not injected: check your FXML file 'WarehouseEditProductView.fxml'.";
        productService = new ProductService();
    }
}
