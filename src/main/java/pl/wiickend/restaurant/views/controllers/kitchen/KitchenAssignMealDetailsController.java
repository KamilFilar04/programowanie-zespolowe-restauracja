package pl.wiickend.restaurant.views.controllers.kitchen;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.MealOrder;
import pl.wiickend.restaurant.entity.MealProduct;
import pl.wiickend.restaurant.services.entity.MealOrderService;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class KitchenAssignMealDetailsController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text mealName;

    @FXML
    private Text mealAmount;

    @FXML
    private Text mealTime;

    @FXML
    private Text mealDescription;

    @FXML
    private Text mealRecipe;

    @FXML
    private TableView<MealProduct> productList;

    @FXML
    private TableColumn<MealProduct, String> productListName;

    @FXML
    private TableColumn<MealProduct, Float> productListAmount;


    private MealOrder mealOrder;
    MealOrderService mealOrderService = new MealOrderService();

    public void setMealOrder(MealOrder x) {
        this.mealOrder = mealOrderService.getByMealOrder(x.getMeal(), x.getOrder());
        mealTime.setText(mealOrder.getMealTime());
        mealName.setText(mealOrder.getMealNameObj());
        mealAmount.setText(mealOrder.getQuantity().toString());
        mealDescription.setText(mealOrder.getMeal().getDescription());
        mealRecipe.setText(mealOrder.getMeal().getRecipe());
        productList.setItems(FXCollections.observableList(mealOrder.getMeal().getProducts()));
    }


    @FXML
    void close(ActionEvent event) {
        Stage stage = (Stage) mealName.getScene().getWindow();
        stage.close();
    }

    @FXML
    void makeMealDone(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Are you sure");
        alert.setHeaderText("Meal Confirmation");
        alert.setContentText("Is meal done?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            mealOrderService.changeMealOrderStateToDone(mealOrder);
            this.close(event);
        }
    }

    @FXML
    void initialize() {
        assert mealName != null : "fx:id=\"mealName\" was not injected: check your FXML file 'KitchenMealDetailsView.fxml'.";
        assert mealAmount != null : "fx:id=\"mealAmount\" was not injected: check your FXML file 'KitchenMealDetailsView.fxml'.";
        assert mealTime != null : "fx:id=\"mealTime\" was not injected: check your FXML file 'KitchenMealDetailsView.fxml'.";
        assert mealDescription != null : "fx:id=\"mealDescription\" was not injected: check your FXML file 'KitchenMealDetailsView.fxml'.";
        assert mealRecipe != null : "fx:id=\"mealRecipe\" was not injected: check your FXML file 'KitchenMealDetailsView.fxml'.";
        assert productList != null : "fx:id=\"productList\" was not injected: check your FXML file 'KitchenMealDetailsView.fxml'.";

        productListName.setCellValueFactory(new PropertyValueFactory<MealProduct, String>("ProductName"));
        productListAmount.setCellValueFactory(new PropertyValueFactory<MealProduct, Float>("Quantity"));
    }
}
