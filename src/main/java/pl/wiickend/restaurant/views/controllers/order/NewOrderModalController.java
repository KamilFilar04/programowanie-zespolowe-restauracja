package pl.wiickend.restaurant.views.controllers.order;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.entity.Order;
import pl.wiickend.restaurant.services.entity.OrderService;
import pl.wiickend.restaurant.views.controllers.AddDishToOrderModalController;

public class NewOrderModalController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ComboBox<Integer> tablesComboBox;

    @FXML
    private Label totalPrice;

    @FXML
    private Button acceptButton;

    @FXML
    private Button cancelButton;

    @FXML
    private TableView<Meal> dishesTable;

    @FXML
    private TableColumn<Meal, String> name_col;

    @FXML
    private TableColumn<Meal, Float> price_col;

    @FXML
    private TableColumn<Meal, Float> quantity_col;

    @FXML
    private Button addDishButton;

    @FXML
    private Button removeDishesButton;

    OrderService orderSerive = new OrderService();

    @FXML
    void acceptOrder(ActionEvent event) {
        if(tablesComboBox.getValue() != null && !tablesComboBox.getItems().isEmpty()){
            Order newOrder = new Order();
            newOrder.setTable_no(tablesComboBox.getValue());
            newOrder.setTotal(Float.valueOf((totalPrice.getText()).substring(0, totalPrice.getText().length() - 1)));
            for(Meal meal : dishesTable.getItems()){
                newOrder.addMeal(meal);
            }
            orderSerive.save(newOrder);
            Stage stage = (Stage) addDishButton.getScene().getWindow();
            stage.close();
        }
    }

    @FXML
    void cancelOrder(ActionEvent event) {
        Stage stage = (Stage) addDishButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    void addDish(ActionEvent event) {
        URL url=getClass().getResource("/fxml/order/AddDishToOrderModal.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        Parent root = null;
        try {
            root = (Parent) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        AddDishToOrderModalController controller = loader.getController();

        Scene scene = new Scene(root);
        Stage stage = new Stage();

        stage.setTitle("Create new order");
        stage.setScene(scene);
        stage.showAndWait();

        Meal addedDish = controller.getDish();
        addDishToTable(addedDish);

    }

    @FXML
    public void removeDish(ActionEvent event) {
        if(dishesTable.getSelectionModel().getSelectedItem() != null){
            Meal removedMeal = dishesTable.getSelectionModel().getSelectedItem();
            dishesTable.getItems().remove(removedMeal);
            dishesTable.refresh();
            setTotal();
            return;
        }
    }

    public void addDishToTable(Meal addedDish){
            for(int i = 0;i < dishesTable.getItems().size(); i++){
                if(dishesTable.getItems().get(i).getName().equals(addedDish.getName())){
                    Meal editedDish = dishesTable.getItems().get(i);
                    editedDish.setInOrderQuantity(dishesTable.getItems().get(i).getInOrderQuantity()+addedDish.getInOrderQuantity());
                    dishesTable.getItems().remove(dishesTable.getItems().get(i));
                    dishesTable.getItems().add(editedDish);
                    dishesTable.refresh();
                    setTotal();
                    return;
                }
            }
            dishesTable.getItems().add(addedDish);
            dishesTable.refresh();
            setTotal();

    }

    public void setTotal() {
        ArrayList<Float> quantityList = new ArrayList();
        ArrayList<Float> priceList = new ArrayList();

        for (Meal item : dishesTable.getItems()) {
            quantityList.add(quantity_col.getCellObservableValue(item).getValue());
            priceList.add(Float.valueOf(price_col.getCellObservableValue(item).getValue()));
        }
        totalPrice.setText(String.valueOf(weightedSum(quantityList, priceList)) + "$");
    }

    public static int weightedSum(ArrayList<Float> one, ArrayList<Float> two) {
        if (one.size() != two.size())
            throw new IllegalArgumentException("Arrays should be with same size");
        int sum = 0;
        for (int i = 0; i < one.size(); i++)
            sum += one.get(i) * two.get(i);
        return sum;
    }

    @FXML
    void initialize() {
        name_col.setCellValueFactory(new PropertyValueFactory<Meal,String>("name"));
        quantity_col.setCellValueFactory(new PropertyValueFactory<Meal,Float>("inOrderQuantity"));
        price_col.setCellValueFactory(new PropertyValueFactory<Meal,Float>("gross_price"));

        tablesComboBox.getItems().setAll(IntStream.rangeClosed(0, 10).boxed().collect(Collectors.toList()));

    }

    private void closeStage(ActionEvent event) {
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }
}

