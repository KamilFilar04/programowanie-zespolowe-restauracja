package pl.wiickend.restaurant.views.controllers.meal;

import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.entity.MealProduct;
import pl.wiickend.restaurant.entity.Product;
import pl.wiickend.restaurant.services.entity.MealService;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

public class MealDetailsController {

    @FXML
    private Text mealName;

    @FXML
    private Text mealTime;

    @FXML
    private Text mealDescription;

    @FXML
    private Text mealRecipe;

    @FXML
    private TableView<MealProduct> product_table;

    @FXML
    private TableColumn<MealProduct, String> product_col;

    @FXML
    private TableColumn<MealProduct, Float> ammount_col;

    @FXML
    private Button return_btn;

    @FXML
    private Button edit_btn;

    @FXML
    private Button delete_btn;

    Meal selectedMeal;
    MealService mealService;

    @FXML
    void close(ActionEvent event) {
        Stage stage = (Stage) mealRecipe.getScene().getWindow();
        stage.close();
    }

    @FXML
    void delete(ActionEvent event) {
        if(selectedMeal != null){
            Alert alertAdd = new Alert(Alert.AlertType.CONFIRMATION);
            alertAdd.setTitle("Are you sure?");
            alertAdd.setHeaderText("Add new dish to seletced menu!");

            Optional<ButtonType> result = alertAdd.showAndWait();
            if (result.get() == ButtonType.OK){
                try {
                    mealService.delete(selectedMeal.getId());
                    refreshTable();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        Stage stage = (Stage) mealRecipe.getScene().getWindow();
        stage.close();
    }

    @FXML
    void edit(ActionEvent event) throws IOException {
        if(selectedMeal == null) return;
        URL url=getClass().getResource("/fxml/meal/MealsEditView.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        if(loader == null)
        {
            throw new RuntimeException ("Could not find: "+url.toString());
        }
        Parent root = loader.load();
        MealEditController controller=(MealEditController) loader.getController();
        controller.setMeal(selectedMeal);
        Scene scene = new Scene(root);
        Stage stage = (Stage) mealRecipe.getScene().getWindow();
        stage.setTitle("Edit Meal");
        stage.setScene(scene);
    }

    public void setMeal(Meal meal) {
        this.selectedMeal = meal;
        mealName.setText(selectedMeal.getName());
        mealTime.setText(Float.toString(selectedMeal.getAverage_preparation_time()));
        mealDescription.setText(selectedMeal.getDescription());
        mealRecipe.setText(selectedMeal.getRecipe());
        product_table.getItems().addAll(selectedMeal.getProducts());
    }

    public void refreshTable(){
        product_table.getItems().removeAll();
        product_table.getItems().addAll(selectedMeal.getProducts());
    }

    @FXML
    void initialize() throws Exception {
        mealService = new MealService();
        product_col.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProductName()));
        ammount_col.setCellValueFactory(new PropertyValueFactory<MealProduct, Float>("quantity"));
    }

}
