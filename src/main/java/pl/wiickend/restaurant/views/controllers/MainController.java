package pl.wiickend.restaurant.views.controllers;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import pl.wiickend.restaurant.views.controllers.kitchen.KitchenController;
import pl.wiickend.restaurant.views.controllers.meal.MealsController;
import pl.wiickend.restaurant.views.controllers.menu.MenuManagerController;
import pl.wiickend.restaurant.views.controllers.menu.MenuWaiterController;
import pl.wiickend.restaurant.views.controllers.user.UsersController;
import pl.wiickend.restaurant.views.controllers.warehouse.WarehouseController;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML
    private TabPane tabPane;

    @FXML
    private MenuManagerController managerFxmlController;

    @FXML
    private MenuWaiterController waiterFxmlController;

    @FXML
    private MealsController mealsFxmlController;

    @FXML
    private WarehouseController warehouseFxmlController;

    @FXML
    private KitchenController kitchenFxmlController;

    @FXML
    private UsersController usersFxmlController;

    @FXML
    private ReportsController reportsFxmlController;

    @FXML
    void initialize() {
        tabPane.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Tab> observable,
                 Tab oldValue, Tab newValue) -> {
                    try {
                        switch (newValue.getId()) {
                            case "manager":
                                managerFxmlController.initialize();
                                break;
                            case "waiter":
                                waiterFxmlController.initialize();
                                break;
                            case "meals":
                                mealsFxmlController.initialize();
                            case "warehouse":
                                warehouseFxmlController.initialize();
                            case "kitchen":
                                kitchenFxmlController.initialize();
                            case "users":
                                usersFxmlController.initialize();
                            case "reports":
                                reportsFxmlController.initialize();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );
    }
}
