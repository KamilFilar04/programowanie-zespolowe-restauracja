package pl.wiickend.restaurant.views.controllers.order;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.entity.MealOrder;
import pl.wiickend.restaurant.entity.MealOrderStatus;
import pl.wiickend.restaurant.entity.Order;
import pl.wiickend.restaurant.services.entity.MealOrderService;
import pl.wiickend.restaurant.services.entity.MealService;
import pl.wiickend.restaurant.services.entity.OrderService;
import pl.wiickend.restaurant.utils.AlertAdder;
import pl.wiickend.restaurant.views.controllers.AddDishToOrderModalController;
import pl.wiickend.restaurant.views.controllers.meal.MealDetailsController;

public class OrderDetailModalController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label total;

    @FXML
    private Button addDishButton;

    @FXML
    private ComboBox<Integer> tableCombo;

    @FXML
    private ComboBox<MealOrderStatus> statusCombo;

    @FXML
    private TableView<MealOrder> dishesTable;

    @FXML
    private TableColumn<MealOrder, String> dishes_id_col;

    @FXML
    private TableColumn<MealOrder, String> dishes_name_col;

    @FXML
    private TableColumn<MealOrder, Float> dishes_quantity_col;

    @FXML
    private TableColumn<MealOrder, String> dishes_price_col;

    @FXML
    private TableColumn<MealOrder, String> dishes_status_col;

    private Order selectedOrder;
    OrderService orderService = new OrderService();
    MealOrderService mealOrderService = new MealOrderService();
    ObservableList<MealOrder> data = FXCollections.observableArrayList();

    @FXML
    void addDish(ActionEvent event) throws IOException {
        URL url = getClass().getResource("/fxml/order/AddDishToOrderModal.fxml");

        FXMLLoader loader = new FXMLLoader(url);
        if (loader == null) {
            throw new RuntimeException("Could not find: " + url.toString());
        }
        Parent root = loader.load();
        AddDishToOrderModalController controller = (AddDishToOrderModalController) loader.getController();
        controller.setOrder(selectedOrder);

        Scene scene = new Scene(root);
        Stage stage = new Stage();

        stage.setTitle("Create new order");
        stage.setScene(scene);
        stage.showAndWait();

        refreshTable();
    }

    @FXML
    void checkoutOrder(ActionEvent event) {
        if (dishesTable.getSelectionModel().getSelectedItem() != null && statusCombo.getValue() != null) {
            MealOrder selectedMealOrder = dishesTable.getSelectionModel().getSelectedItem();
            Order selectedOrder = selectedMealOrder.getOrder();

            selectedMealOrder.setMealOrderStatus(statusCombo.getSelectionModel().getSelectedItem());
            mealOrderService.update(selectedMealOrder);

            if (tableCombo.getValue() != null) {
                selectedOrder.setTable_no(tableCombo.getSelectionModel().getSelectedItem());
                orderService.update(selectedOrder);
                new AlertAdder(Alert.AlertType.INFORMATION, "Table changed", "Table no changed successfully");
            }
            refreshTable();
        }
        if (tableCombo.getValue() != null) {
            new AlertAdder(Alert.AlertType.INFORMATION, "Table changed", "Table no changed successfully");
            selectedOrder.setTable_no(tableCombo.getSelectionModel().getSelectedItem());
            orderService.update(selectedOrder);
            refreshTable();
        }
    }

    @FXML
    void cancelOrder(ActionEvent event) {
        Stage stage = (Stage) addDishButton.getScene().getWindow();
        stage.close();
    }

    private void refreshTable() {
        dishesTable.getColumns().get(0).setVisible(false);
        dishesTable.getColumns().get(0).setVisible(true);
        setTotal();
        selectedOrder = orderService.getById(selectedOrder.getId());
        dishesTable.getItems().clear();
        dishesTable.getItems().addAll(selectedOrder.getMeals());
    }

    @FXML
    void initialize() {
        dishes_id_col.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getMeal().getId())));
        dishes_name_col.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMeal().getName()));
        dishes_price_col.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getMeal().getGross_price())));
        dishes_quantity_col.setCellValueFactory(new PropertyValueFactory<MealOrder, Float>("quantity"));
        dishes_status_col.setCellValueFactory(new PropertyValueFactory<MealOrder, String>("mealOrderStatus"));

        tableCombo.getItems().setAll(IntStream.rangeClosed(0, 10).boxed().collect(Collectors.toList()));
        statusCombo.getItems().addAll(MealOrderStatus.values());

        ObservableList<MealOrder> MealOrderList = FXCollections.observableArrayList();
        List<MealOrder> eList = mealOrderService.loadAll();
        for (MealOrder ent : eList) {
            MealOrderList.add(ent);
        }
        data = MealOrderList;
    }

    public void setOrder(Order selectedOrder) {
        this.selectedOrder = selectedOrder;
        dishesTable.getItems().addAll(selectedOrder.getMeals());
        refreshTable();
    }

    public void setTotal() {
        ArrayList<Float> quantityList = new ArrayList();
        ArrayList<Float> priceList = new ArrayList();

        for (MealOrder item : dishesTable.getItems()) {
            quantityList.add(dishes_quantity_col.getCellObservableValue(item).getValue());
            priceList.add(Float.valueOf(dishes_price_col.getCellObservableValue(item).getValue()));
        }
        total.setText(String.valueOf(weightedSum(quantityList, priceList)) + "$");
    }

    public static int weightedSum(ArrayList<Float> one, ArrayList<Float> two) {
        if (one.size() != two.size())
            throw new IllegalArgumentException("Arrays should be with same size");
        int sum = 0;
        for (int i = 0; i < one.size(); i++)
            sum += one.get(i) * two.get(i);
        return sum;
    }
}
