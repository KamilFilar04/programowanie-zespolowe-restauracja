package pl.wiickend.restaurant.views.controllers.menu;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Menu;
import pl.wiickend.restaurant.services.entity.MenuService;

import java.util.Optional;

public class MenuCreateNewController {

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField descriptionTextField;

//    private MenuManagerController menuManagerController;

    @FXML
    void onSaveMenu(ActionEvent event) throws Exception {
        Alert alertNewMenu = new Alert(Alert.AlertType.CONFIRMATION);
        alertNewMenu.setTitle("Are you sure?");
        alertNewMenu.setHeaderText("After click OK new menu will be create!");

        Optional<ButtonType> result = alertNewMenu.showAndWait();
        if (result.get() == ButtonType.OK){
            System.out.println("Create new menu successful!");

            Menu menu = new Menu(nameTextField.getText(), descriptionTextField.getText(), false);
            MenuService menuService = new MenuService();
            menuService.save(menu);

            this.close();
        }
    }

    @FXML
    void onCancel(ActionEvent event) {
        this.close();
    }

    public void close() {
        Stage stage = (Stage) this.nameTextField.getScene().getWindow();
        stage.close();
    }

    @FXML
    void initialize() {
        assert nameTextField != null : "fx:id=\"nameTextField\" was not injected: check your FXML file 'MenuCreateNew.fxml'.";
        assert descriptionTextField != null : "fx:id=\"descriptionTextField\" was not injected: check your FXML file 'MenuCreateNew.fxml'.";
    }
}
