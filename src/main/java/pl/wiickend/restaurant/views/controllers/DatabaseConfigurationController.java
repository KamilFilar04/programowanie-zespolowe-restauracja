package pl.wiickend.restaurant.views.controllers;

import java.io.*;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DatabaseConfigurationController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button cancelButton;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button saveButton;

    @FXML
    private TextField addressField;

    @FXML
    private TextField loginField;

    private Properties properties;

    final String configFile = System.getProperty("user.home")+"/.config/Restaurant/hibernate.properties";

    @FXML
    void onSaveButtonClick(ActionEvent event) {
        properties.setProperty("hibernate.connection.url", addressField.getText());
        properties.setProperty("hibernate.connection.username", loginField.getText());
        properties.setProperty("hibernate.connection.password", passwordField.getText());

        try {
            OutputStream out = new FileOutputStream(configFile);
            properties.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.closeWindow();
    }

    @FXML
    void onCancelButtonClick(ActionEvent event) {
        this.closeWindow();
    }

    void closeWindow()
    {
        Stage stage = (Stage)loginField.getScene().getWindow();
        stage.close();
    }

    @FXML
    void initialize() {
        assert cancelButton != null : "fx:id=\"cancelButton\" was not injected: check your FXML file 'DatabaseConfiguration.fxml'.";
        assert passwordField != null : "fx:id=\"passwordField\" was not injected: check your FXML file 'DatabaseConfiguration.fxml'.";
        assert saveButton != null : "fx:id=\"saveButton\" was not injected: check your FXML file 'DatabaseConfiguration.fxml'.";
        assert addressField != null : "fx:id=\"addressField\" was not injected: check your FXML file 'DatabaseConfiguration.fxml'.";
        assert loginField != null : "fx:id=\"loginField\" was not injected: check your FXML file 'DatabaseConfiguration.fxml'.";

        File initialFile = new File(configFile);
        try {
            InputStream io = new FileInputStream(initialFile);
            properties = new Properties();
            properties.load(io);
            io.close();

            addressField.setText(properties.get("hibernate.connection.url").toString());
            loginField.setText(properties.get("hibernate.connection.username").toString());
            passwordField.setText(properties.get("hibernate.connection.password").toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
