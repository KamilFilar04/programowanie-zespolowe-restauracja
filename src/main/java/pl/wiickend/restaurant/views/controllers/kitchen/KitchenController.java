package pl.wiickend.restaurant.views.controllers.kitchen;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.MealOrder;
import pl.wiickend.restaurant.entity.MealOrderStatus;
import pl.wiickend.restaurant.entity.User;
import pl.wiickend.restaurant.services.entity.MealOrderService;
import pl.wiickend.restaurant.services.entity.UserService;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class KitchenController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<MealOrder> newMealsTable;

    @FXML
    private TableColumn<MealOrder, String> newMealOrderName;

    @FXML
    private TableColumn<MealOrder, Float> newMealOrderAmount;

    @FXML
    private TableColumn<MealOrder, String> newMealOrderTime;

    @FXML
    private TableView<MealOrder> preperationMealsTable;

    @FXML
    private TableColumn<MealOrder, String> preperationMealName;

    @FXML
    private TableColumn<MealOrder, String> preperationChef;

    @FXML
    private TableColumn<MealOrder, String> preperationTime;

    @FXML
    private TableColumn<MealOrder, Float> preperationAmount;

    @FXML
    private ListView<ChefListElem> chefList;

    public KitchenController() {
    }

    @FXML
    void assignCheff(ActionEvent event) throws IOException {
        if (newMealsTable.getSelectionModel().getSelectedItem() == null) {
            return;
        }
        URL url = getClass().getResource("/fxml/kitchen/KitchenAssignMealView.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        if (loader == null) {
            throw new RuntimeException("Could not find: " + url.toString());
        }
        Parent root = loader.load();
        KitchenAssignMealController controller = (KitchenAssignMealController) loader.getController();
        controller.setMealOrder(newMealsTable.getSelectionModel().getSelectedItem());
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner((Stage) chefList.getScene().getWindow());
        stage.setTitle("Assign Meal To Cheff");
        stage.setScene(scene);
        stage.showAndWait();

        this.updateView();
    }

    @FXML
    void showMealDetails(ActionEvent event) throws IOException {
        if (preperationMealsTable.getSelectionModel().getSelectedItem() == null) {
            return;
        }

        URL url = getClass().getResource("/fxml/kitchen/KitchenMealDetailsView.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        if (loader == null) {
            throw new RuntimeException("Could not find: " + url.toString());
        }
        Parent root = loader.load();
        KitchenAssignMealDetailsController controller = (KitchenAssignMealDetailsController) loader.getController();
        controller.setMealOrder(preperationMealsTable.getSelectionModel().getSelectedItem());
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner((Stage) chefList.getScene().getWindow());
        stage.setTitle("Meal Details");
        stage.setScene(scene);
        stage.showAndWait();

        this.updateView();
    }

    void updateChefList() {
        this.chefList.getItems().clear();
        List<User> users = userService.getUsersWithRole("Chef");
        List<ChefListElem> chefListElemList = new ArrayList<>();

        for (User user : users) {
            chefListElemList.add(new ChefListElem(user));
        }

        ObservableList<ChefListElem> chefListElems = FXCollections.observableArrayList(chefListElemList);
        chefList.setItems(chefListElems);

        chefListElems.forEach(chef -> chef.selectedProperty().addListener((observable, wasSelected, isSelected) -> {
            System.out.println(chef.getUser());
            chef.getUser().setIs_in_work(isSelected);
            userService.update(chef.getUser());
            this.updateChefList();
        }));

    }

    public static class ChefListElem extends User {
        private ReadOnlyStringWrapper customName = new ReadOnlyStringWrapper();
        private BooleanProperty selected = new SimpleBooleanProperty(false);
        private User user;

        public ChefListElem(User user) {
            this.user = user;
            this.selected = new SimpleBooleanProperty(user.getIs_in_work());
            this.customName.set(user.getName() + " " + user.getSurname());
        }

        public String getCustomName() {
            return customName.get();
        }

        public BooleanProperty selectedProperty() {
            return selected;
        }

        public User getUser() {
            return user;
        }

        @Override
        public String toString() {
            return this.getCustomName();
        }
    }

    UserService userService = new UserService();
    MealOrderService mealOrderService = new MealOrderService();

    @FXML
    public void initialize() {
        assert newMealsTable != null : "fx:id=\"newMealsTable\" was not injected: check your FXML file 'KitchenView.fxml'.";
        assert preperationMealsTable != null : "fx:id=\"preperationMealsTable\" was not injected: check your FXML file 'KitchenView.fxml'.";
        assert chefList != null : "fx:id=\"chefList\" was not injected: check your FXML file 'KitchenView.fxml'.";
        assert preperationMealName != null : "preperationMealName is null";
        assert preperationAmount != null : "preperationAmount is null";
        assert preperationChef != null : "preperationChef is null";
        assert preperationTime != null : "preperationTime is null";

        chefList.setCellFactory(CheckBoxListCell.forListView(ChefListElem::selectedProperty));

        newMealOrderName.setCellValueFactory(new PropertyValueFactory<MealOrder, String>("MealNameObj"));
        newMealOrderAmount.setCellValueFactory(new PropertyValueFactory<MealOrder, Float>("Quantity"));
        newMealOrderTime.setCellValueFactory(new PropertyValueFactory<MealOrder, String>("MealTime"));

        preperationMealName.setCellValueFactory(new PropertyValueFactory<MealOrder, String>("MealNameObj"));
        preperationAmount.setCellValueFactory(new PropertyValueFactory<MealOrder, Float>("Quantity"));
        preperationChef.setCellValueFactory(new PropertyValueFactory<MealOrder, String>("ChefName"));
        preperationTime.setCellValueFactory(new PropertyValueFactory<MealOrder, String>("MealTime"));

        this.updateView();
    }

    private void updateView() {
        this.updateChefList();
        this.updateNewMealsList();
        this.updatePreparationMealsList();
    }

    private void updatePreparationMealsList() {
        preperationMealsTable.getItems().clear();
        preperationMealsTable.setItems(FXCollections.observableList(mealOrderService.getMealOrdersWithStatus(MealOrderStatus.preparing)));
    }

    private void updateNewMealsList() {
        newMealsTable.getItems().clear();
        newMealsTable.setItems(FXCollections.observableArrayList(mealOrderService.getMealOrdersWithStatus(MealOrderStatus.newMeal)));
    }
}
