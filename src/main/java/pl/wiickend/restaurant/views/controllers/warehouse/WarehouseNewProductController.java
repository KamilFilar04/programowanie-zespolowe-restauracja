package pl.wiickend.restaurant.views.controllers.warehouse;

        import java.net.URL;
        import java.sql.Date;
        import java.util.Calendar;
        import java.util.ResourceBundle;
        import javafx.event.ActionEvent;
        import javafx.fxml.FXML;
        import javafx.scene.control.Alert;
        import javafx.scene.control.TextField;
        import javafx.stage.Stage;
        import pl.wiickend.restaurant.entity.Product;
        import pl.wiickend.restaurant.services.entity.ProductService;

public class WarehouseNewProductController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField productName;

    @FXML
    private TextField productDescription;

    @FXML
    private TextField productUnit;

    @FXML
    private TextField productQuantity;

    @FXML
    private TextField productNetPrice;

    @FXML
    private TextField productGrossPrice;

    private ProductService productService;

    @FXML
    void addProduct(ActionEvent event) throws Exception {
        Product newProduct = new Product();

        boolean throwErr = false;

        try {
            newProduct.setName(productName.getText());
            newProduct.setDescription(productDescription.getText());
            newProduct.setUnit(productUnit.getText());
            newProduct.setQuantity(Float.valueOf(productQuantity.getText()));
            newProduct.setNet_price(Float.valueOf(productNetPrice.getText()));
            newProduct.setGross_price(Float.valueOf(productGrossPrice.getText()));
            newProduct.setCreatedAt(new Date(Calendar.getInstance().getTime().getTime()));
            newProduct.setUpdatedAt(new Date(Calendar.getInstance().getTime().getTime()));
        }catch (Exception e)
        {
                throwErr = true;
        }

        if(
                throwErr ||
                productName.getText().length()==0 ||
                productUnit.getText().length()==0 ||
                productNetPrice.getText().length()==0 ||
                productGrossPrice.getText().length()==0 ||
                productDescription.getText().length()==0 ||
                productQuantity.getText().length()==0 ||
                Float.parseFloat(productQuantity.getText())<0 ||
                Float.parseFloat(productNetPrice.getText())<0 ||
                Float.parseFloat(productGrossPrice.getText())<0
        )
        {
            Alert a = new Alert(Alert.AlertType.NONE);
            a.setAlertType(Alert.AlertType.ERROR);
            a.show();
            return;
        }
        productService.save(newProduct);
        this.close();
    }

    @FXML
    void closeView(ActionEvent event) {
        this.close();
    }

    void close(){
        Stage stage = (Stage)productName.getScene().getWindow();
        stage.close();
    }

    @FXML
    void initialize() {
        assert productName != null : "fx:id=\"productName\" was not injected: check your FXML file 'WarehouseNewProductView.fxml'.";
        assert productDescription != null : "fx:id=\"productDescription\" was not injected: check your FXML file 'WarehouseNewProductView.fxml'.";
        assert productUnit != null : "fx:id=\"productUnit\" was not injected: check your FXML file 'WarehouseNewProductView.fxml'.";
        assert productQuantity != null : "fx:id=\"productQuantity\" was not injected: check your FXML file 'WarehouseNewProductView.fxml'.";
        assert productNetPrice != null : "fx:id=\"productNetPrice\" was not injected: check your FXML file 'WarehouseNewProductView.fxml'.";
        assert productGrossPrice != null : "fx:id=\"productGrossPrice\" was not injected: check your FXML file 'WarehouseNewProductView.fxml'.";

        productService = new ProductService();
    }
}
