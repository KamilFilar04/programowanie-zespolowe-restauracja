package pl.wiickend.restaurant.views.controllers.menu;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.entity.MealMenu;
import pl.wiickend.restaurant.entity.Menu;
import pl.wiickend.restaurant.entity.Order;
import pl.wiickend.restaurant.services.entity.MealService;
import pl.wiickend.restaurant.services.entity.MenuService;
import pl.wiickend.restaurant.services.entity.OrderService;
import pl.wiickend.restaurant.views.controllers.order.OrderDetailModalController;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MenuWaiterController {


    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<Order> ordersTable;

    @FXML
    private TextField totalSearch;

    @FXML
    private Button searchButton;

    @FXML
    private TableView<Meal> containsOfSelectedMenu;

    @FXML
    private Button newOrderButton;

    @FXML
    private ComboBox<Integer> tableSearch;

    @FXML
    private Button showDetailsButton;

    @FXML
    private SplitMenuButton menusList;

    private Menu currentMenu;
    private Meal currentMeal;

    private MenuService menuService;
    private MealService mealService;

    @FXML
    private TableColumn<Meal, String> selectedMenuDishName;

    @FXML
    private TableColumn<Meal, String> selectedMenuDishPromotion;

    @FXML
    private TableColumn<Meal, String> selectedMenuDishPrice;
    
    @FXML
    private TableColumn<Meal, String> selectedMenuDishPeparationTime;

    @FXML
    private TableColumn<Order, Long> order_id_col;

    @FXML
    private TableColumn<Order, Date> order_pickup_col;

    @FXML
    private TableColumn<Order, Integer> order_table_col;

    @FXML
    private TableColumn<Order, Date> order_time_col;

    @FXML
    private TableColumn<Order, Float> order_total_col;


    @FXML
    void SelectMenuDropdownButton(ActionEvent event) {

    }

    @FXML
    void addNewOrder(ActionEvent event) throws IOException {
        URL url = getClass().getResource("/fxml/order/NewOrderModal.fxml");

        FXMLLoader loader = new FXMLLoader(url);
        if (loader == null) {
            throw new RuntimeException("Could not find: " + url.toString());
        }
        Parent root = loader.load();

        Scene scene = new Scene(root);
        Stage stage = new Stage();

        stage.setTitle("Create new order");
        stage.setScene(scene);
        stage.showAndWait();

        refreshTable();
    }
    @FXML
    void search(ActionEvent event) {

    }

    @FXML
    void showDetails(ActionEvent event) throws IOException {
        URL url=getClass().getResource("/fxml/order/OrderDetailModal.fxml");

        FXMLLoader loader = new FXMLLoader(url);
        if (loader == null) {
            throw new RuntimeException("Could not find: " + url.toString());
        }
        Parent root = loader.load();
        OrderDetailModalController controller = (OrderDetailModalController) loader.getController();
        Order selectedOrder = ordersTable.getSelectionModel().getSelectedItem();
        controller.setOrder(selectedOrder);

        Scene scene = new Scene(root);
        Stage stage = new Stage();

        stage.setTitle("Order details");
        stage.setScene(scene);
        stage.showAndWait();

        try {
            refreshTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshTable() {
        data.clear();
        List<Order> ordersList = orderService.loadAll();
        data = FXCollections.observableList(ordersList);
        ordersTable.setItems(data);
    }

    OrderService orderService = new OrderService();
    Order selectedOrder = null;
    ObservableList<Order> data = FXCollections.observableArrayList();


    @FXML
    public void initialize() throws Exception {

        order_id_col.setCellValueFactory(new PropertyValueFactory<Order, Long>("id"));
        order_table_col.setCellValueFactory(new PropertyValueFactory<Order, Integer>("table_no"));
        order_total_col.setCellValueFactory(new PropertyValueFactory<Order, Float>("total"));

        tableSearch.getItems().setAll(IntStream.rangeClosed(0, 10).boxed().collect(Collectors.toList()));

        ObservableList<Order> OrderList = FXCollections.observableArrayList();
        List<Order> eList = orderService.loadAll();
        for (Order ent : eList) {
            OrderList.add(ent);
        }
        data = OrderList;

        FilteredList<Order> filteredData = new FilteredList<Order>(data, p -> true);

        totalSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(order -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                if (String.valueOf(order.getTotal()).contains(newValue.toLowerCase())) {
                    return true;
                }
                return false;
            });
        });

        tableSearch.valueProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(order -> {
                if (newValue == null) {
                    return true;
                }
                if (order.getTable_no() == (newValue)) {
                    return true;
                }
                return false;
            });
        });

        SortedList<Order> sortedData = new SortedList<Order>(filteredData);
        sortedData.comparatorProperty().bind(ordersTable.comparatorProperty());
        ordersTable.setItems(sortedData);

        selectedMenuDishName.setCellValueFactory(new PropertyValueFactory<Meal, String>("name"));
        selectedMenuDishPrice.setCellValueFactory(new PropertyValueFactory<Meal, String>("gross_price"));
        selectedMenuDishPromotion.setCellValueFactory(new PropertyValueFactory<Meal, String>("promotion"));
        selectedMenuDishPeparationTime.setCellValueFactory(new PropertyValueFactory<Meal, String>("average_preparation_time"));

        menuService = new MenuService();
        mealService = new MealService();

        if(this.setMenusToSplitMenuButtons() != 0) {
            this.setFirstMenuAsCurrent();
        }
    }

    public int setMenusToSplitMenuButtons() throws Exception {
        menusList.getItems().removeAll(menusList.getItems());
        
        List<Menu> menusFromDatabase = menuService.findAllActive();
        for (Menu menu : menusFromDatabase) {
            MenuItem item = new MenuItem(menu.getName());
            item.setId(menu.getId().toString());
            item.setOnAction((event) -> {
                MenuItem clickedItem = (MenuItem)event.getTarget();
                Menu clickedMenu = null;
                try {
                    clickedMenu = menuService.getById(Long.decode(clickedItem.getId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.setCurrentMenu(clickedMenu);
            });
            menusList.getItems().add(item);
        }
        return menusFromDatabase.size();
    }

    private void setFirstMenuAsCurrent() throws Exception {
        String id = menusList.getItems().get(0).getId();
        Menu firstMenu = menuService.getById(Long.decode(id));
        this.setCurrentMenu(firstMenu);
    }

    public void setCurrentMenu(Menu menu) {
        menusList.setText(menu.getName());
        this.setMenuMeals(menu);
        currentMenu = menu;

        currentMeal = null;
    }
    private void setMenuMeals(Menu menu) {
        containsOfSelectedMenu.getItems().clear();

        List<Meal> meals = new ArrayList<Meal>();

        for (MealMenu mealMenu : menu.getMeals()) {
            Meal meal = mealMenu.getMeal();
            meal.setPromotion(mealMenu.getPromotion());
            meals.add(meal);
        }

        for (Meal meal : meals) {
            containsOfSelectedMenu.getItems().add(meal);
        }
    }
}