package pl.wiickend.restaurant.views.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.services.AuthService;

public class LoginController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private PasswordField password;

    @FXML
    private Text errorInfo;

    @FXML
    private TextField username;

    @FXML
    private Button configButton;

    @FXML
    void login(ActionEvent event) {
        try{
            AuthService authService = new AuthService();
            if(authService.login(username.getText(), password.getText())) {
                URL url = null;

                if(Main.loggedUser.getRole().getName().equals("Chef")){
                    url = getClass().getResource("/fxml/kitchen/KitchenView.fxml");
                }

                if(Main.loggedUser.getRole().getName().equals("Manager")){
                    url = getClass().getResource("/fxml/Main.fxml");
                }

                if(Main.loggedUser.getRole().getName().equals("Waiter")){
                    url = getClass().getResource("/fxml/menu/MenuViewWaiter.fxml");
                }

                if(Main.loggedUser.getRole().getName().equals("Warehouseman")){
                    url = getClass().getResource("/fxml/warehouse/WarehouseView.fxml");
                }

                if(url == null){
                    throw new Exception("View for this role wasn't found.");
                }

                FXMLLoader loader = new FXMLLoader(url);
                Parent root = null;
                try {
                    root = (Parent) loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    assert root != null;

                    this.stage.setResizable(true);
                    this.stage.setTitle("Restaurant application");

                    this.stage.setScene(new Scene(root));
                    this.stage.show();

                    this.stage.centerOnScreen();

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            errorInfo.setText(e.getMessage());
            return;
        }

    }

    @FXML
    void onConfigButtonClick(ActionEvent event) {
        URL url=getClass().getResource("/fxml/DatabaseConfiguration.fxml");

        FXMLLoader loader = new FXMLLoader(url);
        Parent root = null;
        try {
            root = (Parent) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            assert root != null;


            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setResizable(true);
            stage.setTitle("Restaurant application");
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.showAndWait();

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private Stage stage;


    @FXML
    void initialize() {
        assert password != null : "fx:id=\"password\" was not injected: check your FXML file 'Login.fxml'.";
        assert username != null : "fx:id=\"username\" was not injected: check your FXML file 'Login.fxml'.";

    }
}
