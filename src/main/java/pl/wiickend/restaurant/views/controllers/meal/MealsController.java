package pl.wiickend.restaurant.views.controllers.meal;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.services.entity.MealService;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

public class MealsController {

    @FXML
    private Label title;

    @FXML
    private TextField search_field;

    @FXML
    private TableView<Meal> table_meals;

    @FXML
    private TableColumn<Meal, Long> col_id;

    @FXML
    private TableColumn<Meal, String> col_meal;

    @FXML
    private TableColumn<Meal, Float> price_col;

    @FXML
    private Button display_btn;

    @FXML
    private Button add_btn;

    @FXML
    private Button edit_btn;

    @FXML
    private Button delete_btn;

    MealService mealService = new MealService();
    Meal selectedMeal = null;
    ObservableList<Meal> data = FXCollections.observableArrayList();

    @FXML
    void createNew(ActionEvent event) throws IOException {
        URL url = getClass().getResource("/fxml/meal/MealsCreateNew.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        if(loader == null)
        {
            throw new RuntimeException ("Could not find: "+url.toString());
        }
        Parent root = loader.load();
        MealCreateNewController controller=(MealCreateNewController) loader.getController();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner((Stage)add_btn.getScene().getWindow());
        stage.setTitle("Create new menu");
        stage.setScene(scene);
        stage.showAndWait();
        try {
            refreshTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void edit(ActionEvent event) throws IOException {
        if(table_meals.getSelectionModel().getSelectedItem() != null) {
            URL url = getClass().getResource("/fxml/meal/MealsEditView.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            if (loader == null) {
                throw new RuntimeException("Could not find: " + url.toString());
            }
            Parent root = loader.load();
            MealEditController controller = (MealEditController) loader.getController();
            Meal selectMeal = table_meals.getSelectionModel().getSelectedItem();
            controller.setMeal(selectMeal);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner((Stage) add_btn.getScene().getWindow());
            stage.setTitle("Create new menu");
            stage.setScene(scene);
            stage.showAndWait();
            try {
                refreshTable();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    void delete(ActionEvent event){
        if(table_meals.getSelectionModel().getSelectedItem() != null){
            Alert alertAdd = new Alert(Alert.AlertType.CONFIRMATION);
            alertAdd.setTitle("Are you sure?");
            alertAdd.setHeaderText("Add new dish to seletced menu!");

            Optional<ButtonType> result = alertAdd.showAndWait();
            if (result.get() == ButtonType.OK){
                try {
                    Meal selectMeal = table_meals.getSelectionModel().getSelectedItem();
                    mealService.delete(selectMeal);
                    refreshTable();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void refreshTable() throws Exception {
        data.clear();

        List<Meal> mealsList = mealService.loadAll();
        data = FXCollections.observableList(mealsList);
        table_meals.setItems(data);
    }

    @FXML
    void display(ActionEvent event) throws IOException {
        URL url=getClass().getResource("/fxml/meal/MealDetailsView.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        if(loader == null)
        {
            throw new RuntimeException ("Could not find: "+url.toString());
        }
        Parent root = loader.load();
        MealDetailsController controller = (MealDetailsController) loader.getController();
        Meal selectMeal = table_meals.getSelectionModel().getSelectedItem();
        if(selectMeal == null) return;
        controller.setMeal(selectMeal);
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner((Stage)table_meals.getScene().getWindow());
        stage.setTitle("Meal Details");
        stage.setScene(scene);
        stage.showAndWait();
        try {
            refreshTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void initialize() throws Exception {
        System.out.println("init meals");
        //each cellValueFactory has been set according to the member variables of your entity class
        col_id.setCellValueFactory(new PropertyValueFactory<Meal, Long>("id"));
        col_meal.setCellValueFactory(new PropertyValueFactory<Meal, String>("name"));
        price_col.setCellValueFactory(new PropertyValueFactory<Meal, Float>("gross_price"));

        ObservableList<Meal> MealList = FXCollections.observableArrayList();
        List<Meal> eList = mealService.loadAll();
        for (Meal ent : eList) {
            MealList.add(ent);
        }
        data = MealList;

        FilteredList<Meal> filteredData = new FilteredList<Meal>(data, p -> true);
        search_field.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(meal -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                if (meal.getName().toLowerCase().contains(newValue.toLowerCase())) {
                    return true;
                }
                return false;
            });
        });

        SortedList<Meal> sortedData = new SortedList<Meal>(filteredData);
        sortedData.comparatorProperty().bind(table_meals.comparatorProperty());
        table_meals.setItems(sortedData);

    }

}
