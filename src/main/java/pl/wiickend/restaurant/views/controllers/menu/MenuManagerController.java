package pl.wiickend.restaurant.views.controllers.menu;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.entity.MealMenu;
import pl.wiickend.restaurant.entity.Menu;
import pl.wiickend.restaurant.services.entity.MealMenuService;
import pl.wiickend.restaurant.services.entity.MealService;
import pl.wiickend.restaurant.services.entity.MenuService;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MenuManagerController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField selectedPromotion;

    @FXML
    private TextField menuName;

    @FXML
    private TableView<MealMenu> selectedMenuDishList;

    @FXML
    private TableView<Meal> dishList;

    @FXML
    private SplitMenuButton menusList;

    @FXML
    private TableColumn<MealMenu, String> selectedMenuDishName;

    @FXML
    private TableColumn<MealMenu, String> selectedMenuPrice;

    @FXML
    private TableColumn<MealMenu, String> selectedMenuPromotion;

    @FXML
    private TableColumn<Meal, String> dishListDishName;

    @FXML
    private TableColumn<Meal, String> dishListDishPrice;

    @FXML
    private CheckBox active;

    @FXML
    private Label selectedDishName;

    @FXML
    private Label selectedDishPrice;

    private Menu currentMenu;
    private Meal currentMeal;

    private MenuService menuService;
    private MealService mealService;
    private MealMenuService mealMenuService;

    @FXML
    void savePromotion(ActionEvent event) throws Exception {
        if (selectedMenuDishList.getSelectionModel().getSelectedItem() == null) {
            return;
        }

        MealMenu updatedMealMenu = selectedMenuDishList.getSelectionModel().getSelectedItem();
        updatedMealMenu.setPromotion(Float.valueOf(selectedPromotion.getText()));

        menuService.update(currentMenu);

        this.updateDishLists();
    }

    @FXML
    void addToMenu(ActionEvent event) throws Exception {
        if (dishList.getSelectionModel().getSelectedItem() == null) {
            return;
        }

        currentMenu.addMeal(dishList.getSelectionModel().getSelectedItem());
        menuService.update(currentMenu);

        this.updateDishLists();
    }

    private void updateDishLists() {
        currentMenu = menuService.getById(currentMenu.getId());

        selectedMenuDishList.getItems().clear();
        selectedMenuDishList.setItems(FXCollections.observableList(currentMenu.getMeals()));

        selectedMenuDishList.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                selectedPromotion.setText(String.valueOf(newSelection.getPromotion() != null ? newSelection.getPromotion() : newSelection.getMealPrice()));
                selectedDishName.setText(newSelection.getMealName());
                selectedDishPrice.setText(String.valueOf(newSelection.getMealPrice()));
            }
        });

        dishList.setItems(FXCollections.observableList(mealService.loadAll()));
        dishList.getItems().removeAll(currentMenu.getMeals().stream().map(Meal::new).collect(Collectors.toList()));

        selectedMenuDishList.getSelectionModel().clearSelection();
        dishList.getSelectionModel().clearSelection();

        selectedPromotion.setText("");
        selectedDishName.setText("");
        selectedDishPrice.setText("");
    }

    private void setCurrentMenu(Menu menu) {
        currentMenu = menuService.getById(menu.getId());
        this.currentMenu = menu;
        this.menuName.setText(menu.getName());
        this.active.setSelected(menu.isActive());
        this.menusList.setText(menu.getName());
    }

    @FXML
    void createNewMenu(ActionEvent event) throws IOException {
        URL url = getClass().getResource("/fxml/menu/MenuCreateNew.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner((Stage) menuName.getScene().getWindow());
        stage.setTitle("Create new menu");
        stage.setScene(scene);
        stage.showAndWait();

        this.updateMenusListAndSelectFirst();
        this.updateDishLists();
    }

    @FXML
    void deleteDishFromMenu(ActionEvent event) throws Exception {
        if (selectedMenuDishList.getSelectionModel().getSelectedItem() == null) {
            return;
        }
        currentMenu.getMeals().remove(selectedMenuDishList.getSelectionModel().getSelectedItem());
        menuService.update(currentMenu);

        this.updateDishLists();
    }

    @FXML
    void isActiveMenu(ActionEvent event) throws Exception {
        currentMenu.setActive(active.isSelected());
        menuService.update(currentMenu);
        setCurrentMenu(currentMenu);
    }

    @FXML
    void deleteThisMenu(ActionEvent event) throws Exception {
        Alert alertDeleteForever = new Alert(Alert.AlertType.CONFIRMATION);
        alertDeleteForever.setTitle("Are you sure?");
        alertDeleteForever.setHeaderText("This menu will be permamently deleted!");

        Optional<ButtonType> result = alertDeleteForever.showAndWait();
        if (result.get() == ButtonType.OK) {
            System.out.println("Menu deleted successful!");

            menuService.delete(currentMenu);
            this.updateMenusListAndSelectFirst();
        }
    }

    private void updateMenusList() {
        List<Menu> menus = menuService.loadAll();
        menusList.getItems().clear();

        for (Menu menu : menus) {
            MenuItem item = new MenuItem();
            item.setId(menu.getId().toString());
            item.setText(menu.getName());

            item.setOnAction((actionEvent -> {
                MenuItem clickedItem = (MenuItem) actionEvent.getTarget();
                setCurrentMenu(menuService.getById(Long.parseLong(clickedItem.getId())));
                this.updateDishLists();
            }));
            menusList.getItems().add(item);
        }
    }


    private void updateMenusListAndSelectFirst() {
        updateMenusList();
        setCurrentMenu(menuService.getById(Long.parseLong(menusList.getItems().get(0).getId())));
        updateDishLists();
    }

    @FXML
    void saveMenuName(ActionEvent event) throws Exception {
        if (menuName.getText() == null || menuName.getText().equals(currentMenu.getName())) {
            return;
        }
        currentMenu.setName(menuName.getText());
        menuService.update(currentMenu);

        updateMenusList();
        setCurrentMenu(currentMenu);
    }

//    private void setMenuMeals(Menu menu) {
//        selectedMenuDishList.getItems().clear();
//
//        List<Meal> meals = new ArrayList<Meal>();
//
//        for (MealMenu mealMenu : menu.getMeals()) {
//            Meal meal = mealMenu.getMeal();
////            meal.setPromotion(0.0f);
//            meals.add(meal);
//        }
//
//        for (Meal meal : meals) {
//            selectedMenuDishList.getItems().add(meal);
//        }
//    }
//
//    private void setMeals(Menu menu) throws Exception {
//        dishList.getItems().removeAll(dishList.getItems());
//
//        List<Meal> meals = mealService.loadAll();
//
//        for (MealMenu meal : menu.getMeals()) {
//            meals.remove(meal.getMeal());
//        }
//
//        dishList.getItems().addAll(meals);
//    }
//
//
//    public int setMenusToSplitMenuButtons() throws Exception {
//        menusList.getItems().removeAll(menusList.getItems());
//
//        List<Menu> menusFromDatabase = menuService.loadAll();
//
//        for (Menu menu : menusFromDatabase) {
//            MenuItem item = new MenuItem(menu.getName());
//            item.setId(menu.getId().toString());
//            item.setOnAction((event) -> {
//                MenuItem clickedItem = (MenuItem)event.getTarget();
//                Menu clickedMenu = null;
//                try {
//                    clickedMenu = menuService.getById(Long.decode(clickedItem.getId()));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                try {
//                    this.setCurrentMenu(clickedMenu);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            });
//            menusList.getItems().add(item);
//        }
//
//        return menusFromDatabase.size();
//
//    }
//
//    @FXML
//    void savePromotion(ActionEvent event) throws Exception {
//        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//        alert.setTitle("Are you sure?");
//        alert.setHeaderText("Save new value of promotion!");
//
//        Optional<ButtonType> result = alert.showAndWait();
//        if (result.get() == ButtonType.OK){
//            System.out.println("Promotion value will be change!");
//            currentMeal.setPromotionInRelation(currentMenu, Float.parseFloat(selectedPromotion.getText()));
//
//            refreshCurrentMeal();
//            refreshCurrentMenu();
//        }
//    }
//
//    @FXML
//    void addToMenu(ActionEvent event) throws Exception {
//        Alert alertAdd = new Alert(Alert.AlertType.CONFIRMATION);
//        alertAdd.setTitle("Are you sure?");
//        alertAdd.setHeaderText("Add new dish to seletced menu!");
//
//        Optional<ButtonType> result = alertAdd.showAndWait();
//        if (result.get() == ButtonType.OK){
//            Meal selectedMeal = dishList.getSelectionModel().getSelectedItem();
//
//            if(selectedMeal != null) {
//                currentMenu.addMeal(selectedMeal);
//                menuService.update(currentMenu);
//
//                this.setCurrentMenu(currentMenu);
//
//                System.out.println("Dish added successful!");
//            } else {
//                Alert error = new Alert(Alert.AlertType.ERROR);
//                error.setTitle("Any selected dish found!");
//                error.setHeaderText("Make sure that you selected dish on dish list!");
//            }
//
//      todo add to menu and update selected menu
//
//        }
//    }

//    @FXML
//    void selectMenuDropdownButton(ActionEvent event) {
//        System.out.println(event.toString());
//    }
//
//    @FXML
//    void createNewMenu(ActionEvent event) throws IOException {
//        URL url = getClass().getResource(".././Views/MenuCreateNew.fxml");
//        FXMLLoader loader = new FXMLLoader(url);
//        Parent root = loader.load();
//        MenuCreateNewController controller = ( MenuCreateNewController) loader.getController();
//        controller.setMenuManagerController(this);
//        Scene scene = new Scene(root);
//        Stage stage = new Stage();
//        stage.initModality(Modality.WINDOW_MODAL);
//        stage.initOwner((Stage)menuName.getScene().getWindow());
//        stage.setTitle("Create new menu");
//        stage.setScene(scene);
//        stage.showAndWait();
//    }


    @FXML
    public void initialize() throws Exception {
        System.out.println("init manager menu");

        assert selectedPromotion != null : "fx:id=\"selectedPromotion\" was not injected: check your FXML file 'MenuViewManager.fxml'.";
        assert menuName != null : "fx:id=\"menuName\" was not injected: check your FXML file 'MenuViewManager.fxml'.";
        assert selectedMenuDishList != null : "fx:id=\"selectedMenuDishList\" was not injected: check your FXML file 'MenuViewManager.fxml'.";
        assert dishList != null : "fx:id=\"dishList\" was not injected: check your FXML file 'MenuViewManager.fxml'.";
        assert menusList != null : "fx:id=\"menusList\" was not injected: check your FXML file 'MenuViewManager.fxml'.";

        dishListDishName.setCellValueFactory(new PropertyValueFactory<Meal, String>("name"));
        dishListDishPrice.setCellValueFactory(new PropertyValueFactory<Meal, String>("gross_price"));

        selectedMenuDishName.setCellValueFactory(new PropertyValueFactory<MealMenu, String>("MealName"));
        selectedMenuPrice.setCellValueFactory(new PropertyValueFactory<MealMenu, String>("MealPrice"));
        selectedMenuPromotion.setCellValueFactory(new PropertyValueFactory<MealMenu, String>("Promotion"));

        menuService = new MenuService();
        mealService = new MealService();

        this.updateMenusListAndSelectFirst();
        this.updateDishLists();
    }
}
