package pl.wiickend.restaurant.services.entity;

import pl.wiickend.restaurant.entity.Product;
import pl.wiickend.restaurant.entity.ProductOrder;
import pl.wiickend.restaurant.entity.ProductOrderStatus;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.utils.Dao;

public class ProductOrderService extends Dao<ProductOrder> {

    public void updateStatus(ProductOrder productOrder, ProductOrderStatus productOrderStatus) {
        if (productOrderStatus == ProductOrderStatus.done) {
            this.endOrder(productOrder);
        } else {
            Main.database.openCurrentSessionWithTransaction();
            productOrder.setProductOrderStatus(productOrderStatus);
            Main.database.getCurrentSession().update(productOrder);
            Main.database.closeCurrentSessionWithTransaction();
        }
    }

    private void endOrder(ProductOrder productOrder) {
        Main.database.openCurrentSessionWithTransaction();
        productOrder.setProductOrderStatus(ProductOrderStatus.done);

        Main.database.getCurrentSession().update(productOrder);

        Product updatedProduct = productOrder.getProduct();
        updatedProduct.setQuantity(updatedProduct.getQuantity() + productOrder.getQuantity());

        Main.database.getCurrentSession().update(updatedProduct);

        Main.database.closeCurrentSessionWithTransaction();
    }
}
