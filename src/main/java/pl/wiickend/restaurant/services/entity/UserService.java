package pl.wiickend.restaurant.services.entity;

import pl.wiickend.restaurant.entity.MealOrder;
import pl.wiickend.restaurant.entity.MealOrderStatus;
import pl.wiickend.restaurant.entity.Role;
import pl.wiickend.restaurant.entity.User;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.utils.Dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;

public class UserService extends Dao<User> {

    public List<User> getUsersWithRole(String roleName) {

        RoleService rs = new RoleService();
        Optional<Role> role = rs.loadAll().stream().filter(obj -> Objects.equals(obj.getName(), roleName)).findFirst();

        List<User> users = new ArrayList<>();

        if (!role.isPresent()) {
            Main.logger.log(Level.SEVERE, "getUsersWithRole Error: did not find a role with name : " + roleName);
        } else {
            Main.database.openCurrentSessionWithTransaction();
            users = Main.database.getCurrentSession().createQuery("SELECT u FROM User u WHERE role = :role", User.class).setParameter("role", role.get()).getResultList();
            Main.database.closeCurrentSessionWithTransaction();
        }
        return users;
    }

    public List<User> getChefsInWork() {
        List<User> users;
        Main.database.openCurrentSessionWithTransaction();
        users = Main.database.getCurrentSession()
                .createQuery("SELECT u FROM User u WHERE role.name = :role AND is_in_work = :is_in_work", User.class)
                .setParameter("role", "Chef")
                .setParameter("is_in_work", true)
                .getResultList();
        Main.database.closeCurrentSessionWithTransaction();
        return users;
    }

    public List<User> getChefsWithoutAssignedMeal() {
        try {
            Main.database.openCurrentSessionWithTransaction();
            List<MealOrder> mealsInPreperation = Main.database.getCurrentSession()
                    .createQuery("SELECT x FROM MealOrder x WHERE x.mealOrderStatus = :status", MealOrder.class)
                    .setParameter("status", MealOrderStatus.preparing)
                    .getResultList();

            List<User> allChefsWithoutDish = Main.database.getCurrentSession()
                    .createQuery("SELECT u FROM User u WHERE role.name = :role AND is_in_work = :is_in_work", User.class)
                    .setParameter("role", "Chef")
                    .setParameter("is_in_work", true)
                    .getResultList();
            Main.database.closeCurrentSessionWithTransaction();

            for (MealOrder x : mealsInPreperation) {
                allChefsWithoutDish.remove(x.getChef());
            }
            return allChefsWithoutDish;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ArrayList<User>();
    }
}