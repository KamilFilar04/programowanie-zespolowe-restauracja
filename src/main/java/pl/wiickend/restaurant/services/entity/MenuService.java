package pl.wiickend.restaurant.services.entity;

import pl.wiickend.restaurant.entity.Menu;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.utils.Dao;

import java.util.ArrayList;
import java.util.List;

public class MenuService extends Dao<Menu> {

    public List<Menu> findAllActive() {
        try {
            Main.database.openCurrentSessionWithTransaction();
            List<Menu> menus = Main.database.getCurrentSession()
                    .createQuery("SELECT m FROM Menu m WHERE active = :active", Menu.class).setParameter("active", true)
                    .getResultList();
            Main.database.closeCurrentSessionWithTransaction();
            return menus;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ArrayList<Menu>();
    }

}
