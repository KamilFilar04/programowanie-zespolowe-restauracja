package pl.wiickend.restaurant.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MealMenuId implements Serializable {
 
    @Column(name = "meal_id")
    private Long mealId;
 
    @Column(name = "menu_id")
    private Long menuId;
 
    private MealMenuId() {}
 
    public MealMenuId(
        Long mealId,
        Long menuId) {
        this.mealId = mealId;
        this.menuId = menuId;
    }
 
    //Getters omitted for brevity
 
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass())
            return false;
 
        MealMenuId that = (MealMenuId) o;
        return Objects.equals(mealId, that.mealId) &&
               Objects.equals(menuId, that.menuId);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(mealId, mealId);
    }
}
