package pl.wiickend.restaurant.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity(name = "MealOrder")
@Table(name = "orders_meals")
public class MealOrder {

    @EmbeddedId
    private MealOrderId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("mealId")
    private Meal meal;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("orderId")
    private Order order;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    private MealOrderStatus mealOrderStatus;

    @Transient
    private String mealName;

    @Transient
    private Float price;

    private Float quantity;

    @OneToOne(fetch = FetchType.LAZY)
    private User chef;

    private LocalDateTime preparationStart;
    private LocalDateTime preparationEnd;

    public LocalDateTime getPreparationEnd() {
        return preparationEnd;
    }

    public void setPreparationEnd(LocalDateTime preparationEnd) {
        this.preparationEnd = preparationEnd;
    }

    public LocalDateTime getPreparationStart() {
        return preparationStart;
    }

    public void setPreparationStart(LocalDateTime preparationStart) {
        this.preparationStart = preparationStart;
    }

    public User getChef() {
        return chef;
    }

    public void setChef(User chef) {
        this.chef = chef;
    }

    public MealOrder() {
    }

    public MealOrder(Meal meal, Order order) {
        this.id = new MealOrderId(meal.getId(), order.getId());
        this.mealName = meal.getName();
        this.meal = meal;
        this.order = order;
        this.quantity = meal.inOrderQuantity;
        this.price = meal.getGross_price();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MealOrder mealOrder = (MealOrder) o;
        return Objects.equals(meal, this.meal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meal);
    }

    public MealOrderId getId() {
        return id;
    }

    public String getMealTime() {
        Map<Float, Float> assoc = new HashMap<Float, Float>();
        assoc.put(1f, 1f);
        assoc.put(2f, 1.5f);
        assoc.put(3f, 1.75f);
        if (this.getQuantity() >= 4) {
            assoc.put(this.getQuantity(), (this.getQuantity() / 2f));
        }

        Float totalTime = assoc.get(this.getQuantity()) * this.meal.getAverage_preparation_time();

        return totalTime.toString();
    }

    public void setId(MealOrderId id) {
        this.id = id;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public MealOrderStatus getMealOrderStatus() {
        return mealOrderStatus;
    }

    public void setMealOrderStatus(MealOrderStatus mealOrderStatus) {
        this.mealOrderStatus = mealOrderStatus;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public String getMealNameObj() {
        return this.meal.getName();
    }

    public String getChefName() {
        return chef.getName() + " " + chef.getSurname();
    }

    @Override
    public String toString() {
        return "MealOrder{" +
                "id=" + id +
                ", mealName=" + getMealName() +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", mealOrderStatus=" + mealOrderStatus.toString() +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
