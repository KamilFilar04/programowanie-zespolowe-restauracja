package pl.wiickend.restaurant.entity;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Objects;

public class MealOrderId implements Serializable {

    @Column(name = "meal_id")
    private Long mealId;

    @Column(name = "order_id")
    private Long orderId;

    private MealOrderId() {

    }

    public MealOrderId(Long mealId, Long orderId) {
        this.mealId = mealId;
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MealOrderId that = (MealOrderId) o;
        return Objects.equals(mealId, that.mealId) &&
                Objects.equals(orderId, that.orderId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mealId, orderId);
    }
}
