package pl.wiickend.restaurant.entity;

public enum MealOrderStatus {
    newMeal, preparing, done, canceled
}
