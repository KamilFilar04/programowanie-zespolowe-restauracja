package pl.wiickend.restaurant.entity;


import pl.wiickend.pdfgenerator.Mappable;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NaturalIdCache;
import org.hibernate.annotations.UpdateTimestamp;
import pl.wiickend.restaurant.services.entity.MealMenuService;
import pl.wiickend.restaurant.services.entity.MealProductService;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.*;

@Entity(name="Product")
@Table(name="products")

@NaturalIdCache
public class Product implements Serializable, Mappable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(unique=true)
    String name;
    String unit;
    Float quantity;
    String description;
    Float net_price;
    Float gross_price;

    @Transient
    Float inMealQuantity;

    @OneToMany(
            mappedBy = "product",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    List<MealProduct> meals = new ArrayList<>();

    @OneToMany(
            mappedBy = "product",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    List<ProductOrder> orders = new ArrayList<>();

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    public Product() {
    }

    public Product(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.unit = product.getUnit();
        this.quantity = product.getQuantity();
        this.description = product.getDescription();
        this.net_price = product.getNet_price();
        this.gross_price = product.getGross_price();
        this.orders = product.getOrders();
    }

    public Product(Long id, String name, String unit, Float quantity, String description, Float net_price, Float gross_price, List<MealProduct> meals, List<ProductOrder> orders) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.quantity = quantity;
        this.description = description;
        this.net_price = net_price;
        this.gross_price = gross_price;
        this.meals = meals;
        this.orders = orders;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Float getInMealQuantity() {
        return inMealQuantity;
    }

    public void setInMealQuantity(Float inMealQuantity) {
        this.inMealQuantity = inMealQuantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getNet_price() {
        return net_price;
    }

    public void setNet_price(Float net_price) {
        this.net_price = net_price;
    }

    public Float getGross_price() {
        return gross_price;
    }

    public void setGross_price(Float gross_price) {
        this.gross_price = gross_price;
    }

    public List<MealProduct> getMeals() {
        return meals;
    }

    public void setMeals(List<MealProduct> meals) {
        this.meals = meals;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<ProductOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<ProductOrder> orders) {
        this.orders = orders;
    }

    public void setQuantityInRelation(Meal meal, Float quantity) {
        for (Iterator<MealProduct> iterator = meals.iterator();
             iterator.hasNext(); ) {
            MealProduct mealProduct = iterator.next();

            if (mealProduct.getMeal().equals(this) &&
                    mealProduct.getMeal().equals(meal)) {
                mealProduct.setQuantity(quantity);
                MealProductService mealMealService = new MealProductService();
                mealMealService.update(mealProduct);
            }
        }
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", quantity=" + quantity +
                ", description='" + description + '\'' +
                ", net_price=" + net_price +
                ", gross_price=" + gross_price +
                ", quantity=" + quantity +
                ", meals=" + meals +
                ", orders=" + orders +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", orders=" + orders +
                '}';
    }

    @Override
    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put("Name", name);
        map.put("Net Price", net_price.toString());
        map.put("Quantity", quantity.toString());
        return map;
    }
}
