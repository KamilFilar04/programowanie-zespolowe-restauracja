package pl.wiickend.restaurant.entity;

import java.io.Serializable;

public class Pdf implements Serializable {
    private String name;
    private String date;
    private String path;
    private long dateMilis;

    public Pdf(String name, String date, String path, long dateMilis) {
        this.name = name;
        this.date = date;
        this.path = path;
        this.dateMilis = dateMilis;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public long getDateMilis() {
        return dateMilis;
    }
}
