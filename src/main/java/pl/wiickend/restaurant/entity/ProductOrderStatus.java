package pl.wiickend.restaurant.entity;

public enum ProductOrderStatus {
    ordered, done, canceled
}
