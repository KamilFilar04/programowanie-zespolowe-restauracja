package pl.wiickend.restaurant.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="products_orders")
public class ProductOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Date order_date;
    Float quantity;
    Float price;

    @Enumerated(EnumType.STRING)
    private ProductOrderStatus productOrderStatus;

    @CreationTimestamp
    Date created_at;

    @UpdateTimestamp
    Date updated_at;

    @ManyToOne(fetch = FetchType.LAZY)
    Product product;

    public ProductOrder() {}

    public ProductOrder(Long id, Float quantity, Float price, ProductOrderStatus productOrderStatus, Date created_at, Date updated_at, Product product) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
        this.productOrderStatus = productOrderStatus;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public ProductOrderStatus getProductOrderStatus() {
        return productOrderStatus;
    }

    public void setProductOrderStatus(ProductOrderStatus productOrderStatus) {
        this.productOrderStatus = productOrderStatus;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Date getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }

    @Override
    public String toString() {
        return "ProductOrder{" +
                "id=" + id +
                ", order_date=" + order_date +
                ", quantity=" + quantity +
                ", price=" + price +
                ", productOrderStatus=" + productOrderStatus +
                ", created_at=" + created_at +
                ", updated_at=" + updated_at +
                ", product=" + product.toString() +
                '}';
    }
}
