package pl.wiickend.restaurant;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.hibernate.Session;
import pl.wiickend.restaurant.entity.User;
import pl.wiickend.restaurant.utils.Database;
import pl.wiickend.restaurant.views.controllers.LoginController;

import java.util.logging.Logger;

public class Main extends Application {
    public static Session databaseSession;
    public static User loggedUser;
    public static Database database;
    public static Logger logger = Logger.getLogger("global");
    public static String PDF_DEST = System.getProperty("user.home") + "/.config/Restaurant/Pdfs";


    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Login.fxml"));
        Parent root = (Parent)loader.load();

        LoginController controller = (LoginController) loader.getController();
        controller.setStage(primaryStage);

        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.centerOnScreen();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
