package pl.wiickend;

import org.junit.jupiter.api.Test;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.entity.Product;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.services.entity.MealService;
import pl.wiickend.restaurant.utils.Database;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
public class MealTest {

    @Test
    public void testCreateMeal() throws Exception {
        Main.database = new Database();
        MealService mealService = new MealService();
        List<Product> products = null;
        Meal meal = new Meal("Testowe spaghetti","500",50.0F,55.0F,
                25.0F,"Cook pasta",products);
        mealService.save(meal);

        List<Meal> listOfMeals = mealService.loadAll();
        for(int i=0; i<listOfMeals.size(); i++) {
            boolean result;
            if(listOfMeals.get(i).getId().equals(meal.getId())) {
                result = true;
                assertTrue(result);
            } else {
                result = false;
            }
        }

        mealService.delete(meal);
    }

    @Test
    public void testUpdateMeal() throws Exception {
        Main.database = new Database();
        MealService mealService = new MealService();
        java.util.Date date = new java.util.Date();
        java.sql.Date date1 = new java.sql.Date(date.getTime());
        List<Product> products = null;
        Meal meal = new Meal("Testowe leczo","500",50.0F,55.0F,
                25.0F,"cook sausage and paprika",products);
        mealService.save(meal);

        String newCalories = "800";


        meal.setCalories(newCalories);
        mealService.update(meal);

        List<Meal> listOfMeals = mealService.loadAll();

        for(int i=0; i<listOfMeals.size(); i++) {
            boolean result;
            System.out.println(listOfMeals.get(i));
            if(listOfMeals.get(i).getCalories().equals(newCalories)) {
                result = true;
                assertTrue(result);
            }
        }

        mealService.delete(meal);

    }

    @Test
    public void testDeleteMeal() throws Exception {
        Main.database = new Database();
        MealService mealService = new MealService();
        java.util.Date date = new java.util.Date();
        java.sql.Date date1 = new java.sql.Date(date.getTime());
        List<Product> products = null;
        Meal meal = new Meal("Testowe danie","500",50.0F,55.0F,
                25.0F,"recipe",products);
        mealService.save(meal);

        mealService.delete(meal);

        List<Meal> listOfMealsAfterDelete = mealService.loadAll();
        boolean result = listOfMealsAfterDelete.contains(meal.getId());
        assertFalse(result);

    }
}
