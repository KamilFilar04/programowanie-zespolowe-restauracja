package pl.wiickend;

import org.junit.jupiter.api.Test;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.entity.MealProduct;
import pl.wiickend.restaurant.entity.Product;
import pl.wiickend.restaurant.entity.ProductOrder;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.services.entity.ProductService;
import pl.wiickend.restaurant.utils.Database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class ProductTest {

    @Test
    public void testCreateProduct() throws Exception {
        Main.database = new Database();
        ProductService productService = new ProductService();
        long id = 505;
        List<MealProduct> meals = null;
        List<ProductOrder> orders = null;
        Product product = new Product(id,"test Cheese","kg",50.0F,"yellow cheese",50.0F,
                55.0F,meals,orders);

        productService.save(product);

        List<Product> listOfProducts = productService.loadAll();

        for(int i=0; i<listOfProducts.size(); i++) {
            boolean result;
            if(listOfProducts.get(i).getId().equals(product.getId())) {
                result = true;
                assertTrue(result);
            } else {
                result = false;
            }
        }

        productService.delete(product);

    }

    @Test
    public void testUpdateProduct() throws Exception {
        Main.database = new Database();
        ProductService productService = new ProductService();
        long id = 555;
        java.util.Date date = new java.util.Date();
        java.sql.Date date1 = new java.sql.Date(date.getTime());
        List<MealProduct> meals = null;
        List<ProductOrder> orders = null;
        Product product = new Product(id,"test white Cheese","kg",50.0F,"yellow cheese",50.0F,
                55.0F,meals,orders);

        productService.save(product);

        String newName = "White cheese";
        product.setName(newName);
        productService.update(product);

        List<Product> listOfProducts = productService.loadAll();

        for(int i=0; i<listOfProducts.size(); i++) {
            boolean result;
            if(listOfProducts.get(i).getName().equals(newName)) {
                result = true;
                assertTrue(result);
            } else {
                result = false;
            }
        }

        productService.delete(product);
    }

    @Test
    public void testDeleteProduct() throws Exception {
        Main.database = new Database();
        ProductService productService = new ProductService();
        long id = 707;
        java.util.Date date = new java.util.Date();
        java.sql.Date date1 = new java.sql.Date(date.getTime());
        List<MealProduct> meals = null;
        List<ProductOrder> orders = null;
        Product product = new Product(id,"test fish","kg",50.0F,"big fish",50.0F,
                55.0F,meals,orders);
        productService.save(product);

        productService.delete(product);

        List<Product> listOfProductsAfterDelete = productService.loadAll();
        boolean result = listOfProductsAfterDelete.contains(product.getId());
        assertFalse(result);

    }
}
