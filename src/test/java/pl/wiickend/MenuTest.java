package pl.wiickend;

import org.junit.jupiter.api.Test;
import pl.wiickend.restaurant.entity.Menu;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.services.entity.MenuService;
import pl.wiickend.restaurant.utils.Database;
import static org.junit.jupiter.api.Assertions.*;
import java.util.List;

public class MenuTest {

    @Test
    public void testCreateMenu() throws Exception {
        Main.database = new Database();
        MenuService menuService = new MenuService();
        Menu menu = new Menu("Menu wakacyjno-testowe","Testowe menu wakacyjne",true);
        menuService.save(menu);

        List<Menu> listOfMenus = menuService.loadAll();
        for(int i=0 ; i<listOfMenus.size(); i++) {
            boolean result;
            if(listOfMenus.get(i).getId().equals(menu.getId())) {
                result = true;
                assertTrue(result);
            } else {
                result = false;
            }
        }

        menuService.delete(menu);
    }

    @Test
    public void testUpdateMenu() throws Exception {
        Main.database = new Database();
        MenuService menuService = new MenuService();
        Menu menu = new Menu("Menu testowe","Testowe menu do aktualizacji",true);
        menuService.save(menu);

        String newName = "Menu testowe update";
        menu.setName(newName);
        menuService.update(menu);

        List<Menu> listOfMenus = menuService.loadAll();

        for(int i=0; i<listOfMenus.size(); i++) {
            boolean result;
            if(listOfMenus.get(i).getName().equals(newName)) {
                result = true;
                assertTrue(result);
            } else {
                result = false;
            }
        }

        menuService.delete(menu);
    }

    @Test
    public void testDeleteMenu() throws Exception {
        Main.database = new Database();
        MenuService menuService = new MenuService();
        Menu menu = new Menu("Test menu delete","Testowe menu do usuniecia",true);
        menuService.save(menu);

        menuService.delete(menu);

        List<Menu> listOfMenusAfterDelete = menuService.loadAll();
        boolean result = listOfMenusAfterDelete.contains(menu.getName());
        assertFalse(result);

    }

}
