package pl.wiickend;

import org.junit.jupiter.api.Test;
import pl.wiickend.restaurant.entity.Role;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.services.entity.RoleService;
import pl.wiickend.restaurant.utils.Database;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class RoleTest {


    @Test
    public void testCreateRole() throws Exception {
        Main.database = new Database();
        RoleService roleService = new RoleService();
        Role role = new Role("new waiter");
        roleService.save(role);

        List<Role> listOfRoles = roleService.loadAll();
        for(int i=0; i<listOfRoles.size(); i++) {
            boolean result;
            if(listOfRoles.get(i).getName().equals(role.getName())) {
                result = true;
                assertTrue(result);
            } else {
                result = false;
            }

        }

        roleService.delete(role);
    }

    @Test
    public void testUpdateRole() throws Exception {
        Main.database = new Database();
        RoleService roleService = new RoleService();
        Role role = new Role("new chef");
        roleService.save(role);

        String newName = "updated chef";
        role.setName(newName);
        roleService.update(role);

        List<Role> listOfRoles = roleService.loadAll();

        for(int i=0; i<listOfRoles.size(); i++) {
            boolean result;
            if(listOfRoles.get(i).getName().equals(newName)) {
                result = true;
                assertTrue(result);
            } else {
                result = false;
            }

        }
        roleService.delete(role);
    }

    @Test
    public void testDeleteRole() throws Exception {
        Main.database = new Database();
        RoleService roleService = new RoleService();
        Role role = new Role("new manager");
        roleService.save(role);

        roleService.delete(role);

        List<Role> listOfRolesAfterDelete = roleService.loadAll();
        boolean result = listOfRolesAfterDelete.contains(role.getName());
        assertFalse(result);

    }
}
